import 'package:flutter/material.dart';

import 'GenerateScreen.dart';
import 'Scan.dart';



class HomeScreen extends StatelessWidget{

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        centerTitle: true,
        title: Text('QR Code Reader & Generator'),
      ),
      body: Center(
        child:Column(
          mainAxisAlignment: MainAxisAlignment.center,
          crossAxisAlignment: CrossAxisAlignment.stretch,
          children: <Widget>[
            Padding(
              padding: EdgeInsets.symmetric(vertical: 8.0,horizontal: 16.0),
              child: RaisedButton(
                color: Colors.blue,
                textColor: Colors.white,
                onPressed: (){
                  Navigator.push(
                    context,
                    MaterialPageRoute(builder: (context) => Scan()),
                  );
                },
                child: const Text('Scan QR Code'),
              ),
            ),
            Padding(
              padding: EdgeInsets.symmetric(vertical: 8.0, horizontal: 16.0),
               child: RaisedButton(
               color: Colors.blue,
               textColor: Colors.white,
               onPressed: () {
                 Navigator.push(
                     context,
                     MaterialPageRoute(builder:
                         (context) => GenerateScreen())
                 );
               },
                 child: const Text('Generate QR code'),

               ),
            ),

          ],
        )


        ),
      );


  }
}